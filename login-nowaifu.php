<!DOCTYPE html>
<html>
<head>
	<title>Login ~ NoWaifu</title>
	<link rel="stylesheet" type="text/css" href="nav-style.css">
	<link rel="stylesheet" type="text/css" href="login-style.css">	
</head>
<body>
	<div id="nav-cont">
		
		<div class="clear"></div>
		<nav id="nav">
			<a href="login-nowaifu.php">Inicio</a>
			<a href="quienes-somos.php">Quienes somos</a>
			<a href="nowaifu-FAQ.php">FAQ</a>
		</nav>
		<img src="img/web-img/wafle-logo.png" id="nowaifu-logo" />
	</div>
	<div class="clear"></div>
	<div id="cuerpo">
		<div id="login">
			<form action="nowaifu_main.php">
				<table align="center">
					<tr>
						<td colspan="2"><h1>LOGIN</h1></td>
					</tr>
					<tr>
						<td><input type="text" name="log-username" placeholder="Username" required></td>
					</tr>
					<tr>
						<td><input type="Password" name="log-password" placeholder="Password" required></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" name="enter" value="Enter" class="boton"></td>
					</tr>
				</table>
			</form>
			<div class="clear"></div>
		</div>
		<div id="register">
			<form>
				<table align="center">
					<tr>
						<td colspan="1">
							<h1>REGISTER</h1>
						</td>
					</tr>
					<tr>
						<td><input type="text" name="reg-username" placeholder="Username ..." required></td>
					</tr>
					<tr>
						<td><input type="Password" name="reg-password" placeholder="Password ..." required></td>
					</tr>
					<tr>
						<td><input type="Email" name="reg-email" placeholder="Email ... " required></td>
					</tr>
					<tr>
						<td ><input type="submit" name="enviar" value="Register" class="boton"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="clear"></div>
	</div>
</body>
</html>